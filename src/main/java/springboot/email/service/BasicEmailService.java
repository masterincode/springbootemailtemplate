package springboot.email.service;

import java.io.File;
import java.util.ResourceBundle;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class BasicEmailService { 
	
	@Autowired
    private JavaMailSender emailSender;
 
		 
    public String sendSimpleMessage(String subject, String text) {
    	
    	try
    	{

    		    ResourceBundle s=ResourceBundle.getBundle("application-email");
    		
    			String filename=s.getString("mail.filename");
    			String pathToAttachment=s.getString("mail.attachment")+filename;	
    		    File file = new File(pathToAttachment);
    			
    			MimeMessage message = emailSender.createMimeMessage();
    	        MimeMessageHelper helper = new MimeMessageHelper(message, true);

    	        helper.setSubject(subject);
    	        helper.setText(text);
    	        helper.setTo(s.getString("mail.receiver"));
 	            helper.setFrom(s.getString("spring.mail.username"));
    	        helper.addCc(s.getString("mail.cc"));
    	        helper.addAttachment(filename,file );

    	        emailSender.send(message);
    	        
    	        System.out.println("--------email sent-----------");
    	  
            
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    		return "error";
    	}
          	        
        return "success";
    }
}
    

