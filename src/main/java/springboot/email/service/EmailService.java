package springboot.email.service;



import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.ResourceBundle;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import springboot.email.dto.MailRequest;
import springboot.email.dto.MailResponse;

@Service
public class EmailService {
	
	@Autowired
	private JavaMailSender sender;
	
	@Autowired
	private Configuration config;
	
	public MailResponse sendEmail(MailRequest request, Map<String, Object> model) {
		MailResponse response = new MailResponse();
		MimeMessage message = sender.createMimeMessage(); 
		
		ResourceBundle s=ResourceBundle.getBundle("application-email");
		
		try {
			// set mediaType
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,StandardCharsets.UTF_8.name());
			
			// add attachment
			helper.addAttachment("logo.png", new ClassPathResource("logo.png"));
			
//------------------------ add attachment from your drive---------------------------------------------
			String filename=s.getString("mail.filename");
			String pathToAttachment=s.getString("mail.attachment")+filename;	
		    File file = new File(pathToAttachment); 
			helper.addAttachment(filename,file);
//		----------------------------------------------------------------------------------------------	
			
			Template t = config.getTemplate("EmailBody.html");
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			
			helper.setTo(s.getString("mail.receiver"));
			helper.setText(html, true);
			helper.setSubject(request.getSubject());
			helper.setFrom(s.getString("spring.mail.username"));
			helper.addCc(s.getString("mail.cc"));
			sender.send(message);
			
			response.setMessage("mail send to : " + request.getTo());
			response.setStatus(Boolean.TRUE);

		} catch (MessagingException | IOException | TemplateException e) {
			response.setMessage("Mail Sending failure : "+e.getMessage());
			response.setStatus(Boolean.FALSE);
		}

		return response;
	}
	

}
