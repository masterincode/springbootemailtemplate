package springboot.email.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.email.dto.MailRequest;
import springboot.email.dto.MailResponse;
import springboot.email.service.EmailService;


@RestController
public class EmailController 
{
	
	
	@Autowired
	private EmailService service;

	@RequestMapping("/sendingEmail")
	public MailResponse sendEmail(@RequestBody MailRequest request)
	{
//		http://localhost:8080/springbootemail/sendingEmail
//		This is the body that should pass in postman
//		{ "name":"Angular 7","to":"balmukund.singh@gmail.com","from":"balmukund.singh@gmail.com","subject":"Training of Angular"}
		
		Map<String, Object> model = new HashMap<>();
		model.put("Name", request.getName());
		model.put("location", "Bangalore,India");
		return service.sendEmail(request, model);

	}
	

}
