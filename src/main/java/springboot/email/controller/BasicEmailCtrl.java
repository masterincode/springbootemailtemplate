package springboot.email.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.email.service.BasicEmailService;


@RestController
public class BasicEmailCtrl {

	@Autowired
	private BasicEmailService emailServiceUtils;
	
	@RequestMapping("/send")
	public String sendEmail() 
	{
		System.out.println("----email ctrl---------");
		String res=	emailServiceUtils.sendSimpleMessage("Task is pending","Dear Virendra, Kindly do the needful");
		
		if(res.equalsIgnoreCase("error"))
		{
			return "error";
		}
		else 
		{
			return "success";
		}
		
	}
}
